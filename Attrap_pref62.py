import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_pref62(Attrap):

    # Config
    __HOST = 'https://www.pas-de-calais.gouv.fr'
    __RAA_PAGE = {
        '2024': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2024-Recueils-des-actes-administratifs'
        ],
        '2023': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2023-Recueils-des-actes-administratifs',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2023-Recueils-speciaux-des-actes-administratifs'
        ],
        '2022': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2022-Recueils-des-Actes-Administratifs',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2022-Recueils-Speciaux-des-Actes-Administratifs'
        ],
        '2021': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2021-Recueils-des-actes-administratifs',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2021-Recueils-speciaux-des-actes-administratifs'
        ],
        '2020': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2020-Recueils-des-actes-administratifs',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2020-Recueils-speciaux-des-actes-administratifs'
        ],
        '2019': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2019-Recueil-des-actes-administratifs',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/2019-Recueils-speciaux-des-actes-administratifs'
        ]
    }
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture du Pas-de-Calais'
    short_code = 'pref62'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.enable_tor(20)

    def get_raa(self, keywords):
        pages_to_parse = []
        if self.not_before.year <= 2024:
            for page in self.__RAA_PAGE['2024']:
                pages_to_parse.append(page)
        if self.not_before.year <= 2023:
            for page in self.__RAA_PAGE['2023']:
                pages_to_parse.append(page)
        if self.not_before.year <= 2022:
            for page in self.__RAA_PAGE['2022']:
                pages_to_parse.append(page)
        if self.not_before.year <= 2021:
            for page in self.__RAA_PAGE['2021']:
                pages_to_parse.append(page)
        if self.not_before.year <= 2020:
            for page in self.__RAA_PAGE['2020']:
                pages_to_parse.append(page)
        if self.not_before.year <= 2019:
            for page in self.__RAA_PAGE['2019']:
                pages_to_parse.append(page)

        elements = []
        for raa_page in pages_to_parse:
            page_content = self.get_page(raa_page, 'get').content
            for element in self.get_raa_elements(page_content):
                elements.append(element)

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # On récupère le div qui contient la liste des RAA
        cards = soup.select('div.fr-downloads-group.fr-downloads-group--bordered')[0]
        # On analyse chaque balise a dans ce div
        for a in cards.find_all('a', href=True):
            if a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.find('span').previous_sibling.replace('Télécharger ', '').strip()
                date = datetime.datetime.strptime(a.find('span').get_text().split(' - ')[-1].strip(), '%d/%m/%Y')

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements[::-1]
